module p_wall_mod
USE Variablen
USE sync_mod
USE derivatives_x_mod
USE derivatives_y_mod
USE derivatives_z_mod
USE  sutherland_mod

CONTAINS

subroutine p_wall(dy)


integer:: i,j,k,dummy,n1,n2
real*8 a1,a2,d60y137,dy,del60y
real*8:: line(10)
real*8:: dxtxy(m_local,n_local,o_local),dztzy(m_local,n_local,o_local)
real*8:: tyy(m_local,n_local,o_local)

! Faktoren für kompakte Differenz
      a1=4./3.
      a2=2./3.
      del60y=1./(60.*dy)
      d60y137=1./del60y/137.

!Geschwenigkeiten aus rho_u,rho_v,rho_w
u(1:m_local,1:n_local,1:o_local)=rho_u(1:m_local,1:n_local,1:o_local)&
/rho(1:m_local,1:n_local,1:o_local)
	
v(1:m_local,1:n_local,1:o_local)=rho_v(1:m_local,1:n_local,1:o_local)&
/rho(1:m_local,1:n_local,1:o_local)

w(1:m_local,1:n_local,1:o_local)=rho_w(1:m_local,1:n_local,1:o_local)&
/rho(1:m_local,1:n_local,1:o_local)


!benötigte Spannungstensoren! 

!tau_zy
CALL Dphi_Dy_p(w,delta_w,qyi_i,qyi_nr,qyi_r)

! delta_v berechnen
CALL Dphi_Dz_p(v,delta_v,qzi_i,qzi_nr,qzi_r)

!tau_zy(:,:,:)[:,:,:]
tau_zy(1:m_local,1:n_local,1:o_local)=&
!rho_w(1:m_local,1:n_local,1:o_local)*v(1:m_local,1:n_local,1:o_local)-&
mu(1:m_local,1:n_local,1:o_local)&
*(delta_v(1:m_local,1:n_local,1:o_local)&
+delta_w(1:m_local,1:n_local,1:o_local))
!Spannungstensor neu berechnen
!tau_xy
CALL Dphi_Dx_p(v,delta_v,qxi_i,qxi_nr,qxi_r)
! delta_v berechnen
CALL Dphi_Dy_p(u,delta_u,qyi_i,qyi_nr,qyi_r)

!tau_xy(:,:,:)[:,:,:]
tau_xy(1:m_local,1:n_local,1:o_local)=&
!rho_u(1:m_local,1:n_local,1:o_local)*v(1:m_local,1:n_local,1:o_local)-&
mu(1:m_local,1:n_local,1:o_local)&
*(delta_u(1:m_local,1:n_local,1:o_local)&
+delta_v(1:m_local,1:n_local,1:o_local))

!Spannungstensoren ableiten

CALL Dphi_Dx_p(-tau_xy,delta_u,qxi_i,qxi_nr,qxi_r)

CALL Dphi_Dz_p(-tau_zy,delta_w,qzi_i,qzi_nr,qzi_r)

dxtxy(1:m_local,1:n_local,1:o_local)=delta_u(1:m_local,1:n_local,1:o_local)

dztzy(1:m_local,1:n_local,1:o_local)=delta_w(1:m_local,1:n_local,1:o_local)


!mu neu berechnen
call sutherland

!ABLEITUNGEN
! delta_u berechnen

CALL Dphi_Dx_p(u,delta_u,qxi_i,qxi_nr,qxi_r)
! delta_v berechnen

CALL Dphi_Dy_p(v,delta_v,qyi_i,qyi_nr,qyi_r)

! delta_w berechnen

CALL Dphi_Dz_p(w,delta_w,qzi_i,qzi_nr,qzi_r)

!tyy berechnen

tyy(1:m_local,1,1:o_local)=&
+rho_v(1:m_local,1,1:o_local)*v(1:m_local,1,1:o_local)&
-mu(1:m_local,1,1:o_local)*(a1*delta_v(1:m_local,1,1:o_local)-&                         
    a2*(delta_u(1:m_local,1,1:o_local)+delta_w(1:m_local,1,1:o_local)))

!print *, "Testoutput", delta_u(1,2,1),delta_u(2,2,1),delta_u(3,2,1)

    tyy(1:m_local,2:6,1:o_local)=&
p(1:m_local,2:6,1:o_local)&
+rho_v(1:m_local,2:6,1:o_local)*v(1:m_local,2:6,1:o_local)&
-mu(1:m_local,2:6,1:o_local)*(a1*delta_v(1:m_local,2:6,1:o_local)-&                         
    a2*(delta_u(1:m_local,2:6,1:o_local)+delta_w(1:m_local,2:6,1:o_local)))
! berechne den neuen Druck
do k=1,o_local
       do j=2,m_local-1
         p(j,1,k)=-tyy(j,1,k)  +&   !- drvdt(1,n,k)*d60y137 +
      (300.*tyy(j,2,k)-300.*tyy(j,3,k)+200.*tyy(j,4,k)&
              -75.*tyy(j,5,k)+ 12.*tyy(j,6,k))/137. & 
            +d60y137*dxtxy(j,1,k)+d60y137*dztzy(j,1,k)
	enddo
enddo


end subroutine

end module
