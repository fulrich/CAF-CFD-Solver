module DRUCK_FELD_MOD
! in dieser Subroutine wird das Druckfeld aktualisiert
USE Variablen

contains

subroutine DRUCK_FELD

! p=rho*R*T (ideales Gas-Gesetz)
p(1:m_local,1:n_local,1:o_local)=&
(rho(1:m_local,1:n_local,1:o_local)*287.0)*T(1:m_local,1:n_local,1:o_local)


!test
!T(1,1:n_local,1:o_local)=T_bc(1,1:n_local,1:o_local)

!rho(1,1:n_local,1:o_local)=rho_bc(1,1:n_local,1:o_local)
!do i=2,n_local
!p(1:m_local,1,-1:o_local+2)=p(1:m_local,1,-1:o_local+2)+p(1:m_local,i,-1:o_local+2)
!end do

!p(1:m_local,1,-1:o_local+2)=p(1:m_local,1,-1:o_local+2)/n_local

!do i=1,n_local
!p(1:m_local,i,-1:o_local+2)=p(1:m_local,1,-1:o_local+2)
!end do
end subroutine

end module
