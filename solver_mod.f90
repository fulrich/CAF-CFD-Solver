module solver_mod
USE Variablen
USE xmom_mod
USE ymom_mod
USE zmom_mod
USE energy_mod
USE check_conti_mod
USE conti_mod
USE sync_mod
USE sutherland_mod
USE spannungstensor_mod
USE vel_feld_mod
USE druck_feld_mod
USE temp_feld_mod
USE en_feld_mod
USE bc_mod
contains

subroutine solver(dx,dy)
integer:: i,j,d
real*8::dx,dy
sync all
	!wichtig
	CALL SUTHERLAND
sync all
!print *, "sutherland"
	CALL tensor
!print *, "tensor"
!	CALL GC_EXCHANGE_P 


		!! Lösung mit kompakten Diff.			
!		CALL CHECK_CONTI
sync all
		CALL CONTI 
!print *, "conti"
sync all
		CALL XMOM
!print *, "xmom"
sync all
		CALL YMOM
!print *, "ymom"
sync all
		CALL ZMOM
!print *, "zmom"
sync all
		CALL ENERGY
!print *, "energy"

! Mac Cormac Step
!rho_alt(1:m_local,1:n_local,1:o_local)=rho(1:m_local,1:n_local,1:o_local)
!rho_u_alt(1:m_local,1:n_local,1:o_local)=rho_u(1:m_local,1:n_local,1:o_local)
!rho_v_alt(1:m_local,1:n_local,1:o_local)=rho_v(1:m_local,1:n_local,1:o_local)
!rho_w_alt(1:m_local,1:n_local,1:o_local)=rho_w(1:m_local,1:n_local,1:o_local)
!rho_E_alt(1:m_local,1:n_local,1:o_local)=rho_E(1:m_local,1:n_local,1:o_local)
sync all

rho(1:m_local,1:n_local,1:o_local)=rho_neu(1:m_local,1:m_local,1:m_local)
rho_u(1:m_local,1:n_local,1:o_local)=rho_u_neu(1:m_local,1:n_local,1:o_local)
rho_v(1:m_local,1:n_local,1:o_local)=rho_v_neu(1:m_local,1:n_local,1:o_local)
rho_w(1:m_local,1:n_local,1:o_local)=rho_w_neu(1:m_local,1:n_local,1:o_local)
rho_E(1:m_local,1:n_local,1:o_local)=rho_E_neu(1:m_local,1:n_local,1:o_local)

!CALL VEL_FELD
 

!CALL BC(dx,dy)
	!Energiefelder werden aktualisiert
!CALL EN_FELD

!CALL BC(dx,dy)


	!Temperatur aus Energie- und GGeschwindigkeitsfeld 	
!CALL TEMP_FELD
!T(1:m_local,1:n_local,1:o_local)=p(1:m_local,1:n_local,1:o_local)&
!/rho(1:m_local,1:n_local,1:o_local)/287.0

!CALL BC(dx,dy)

! Druck aus dem idealen Gas-Gesetz
!CALL DRUCK_FELD


!CALL BC(dx,dy)


!	CALL SUTHERLAND

!print *, "sutherland"
!	CALL tensor
!print *, "tensor"
!	CALL GC_EXCHANGE_P 


		!! Lösung mit kompakten Diff.			
!		CALL CHECK_CONTI

!		CALL CONTI 
!print *, "conti"

!		CALL XMOM
!print *, "xmom"
!		CALL YMOM
!print *, "ymom"

!		CALL ZMOM
!print *, "zmom"

!		CALL ENERGY
!print *, "energy"


!Überschreiben der neuen Zustandsvariablen
! in die Alten
!rho(2:m_local,2:n_local,1:o_local)=&
!0.5*(rho(2:m_local,1:n_local,1:o_local)+&
!rho_neu(2:m_local,2:n_local,1:o_local))

!rho_u(2:m_local,2:n_local,1:o_local)=&
!0.5*(rho_u(2:m_local,2:n_local,1:o_local)+&
!rho_u_neu(2:m_local,2:n_local,1:o_local))

!rho_v(2:m_local,2:n_local,1:o_local)=&
!0.5*(rho_v(2:m_local,2:n_local,1:o_local)+&
!rho_v_neu(2:m_local,2:n_local,1:o_local))

!rho_w(2:m_local,2:n_local,1:o_local)=&
!0.5*(rho_w(2:m_local,2:n_local,1:o_local)+&
!rho_w_neu(2:m_local,2:n_local,1:o_local))

!rho_E(2:m_local,2:n_local,1:o_local)=&
!0.5*(rho_E(2:m_local,2:n_local,1:o_local)+&
!rho_E_neu(2:m_local,2:n_local,1:o_local))


end subroutine

end module
