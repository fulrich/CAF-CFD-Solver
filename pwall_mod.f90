
      subroutine pwall(rho,ru,rv,rw,p,drvdt,mu,m1,m2,n1,n2,count,
     &                 out,irk,iq)
c-----------------------------------------------------------------------
c     boundary condition for the pressure from the rv-momentum equation
c     09/30/2002
c     Copyright Christian Stemmer
c-----------------------------------------------------------------------
      use dimensions
      use compact_coefficients
      use size

      implicit none

      integer m,n,k,m1,m2,n1,n2,mmx,nmx,count,irk,iq
      real a1,a2,d60y137
      logical out
      real rho(mmax,nmax,kmax)
      real ru(mmax,nmax,kmax),rv(mmax,nmax,kmax),rw(mmax,nmax,kmax)
      real p(mmax,nmax,kmax)
      real mu(mmax,nmax,kmax),drvdt(1,nmax,kmax)

      real u(m1:m2,n1:n2,kmax),v(m1:m2,n1:n2,kmax),w(m1:m2,n1:n2,kmax)
      real rvv(m1:m2,n1:n2,kmax),v2(1:1,n1:n2,kmax)
      real ux(m1:m2,n1:n2,kmax),uy(m1:m2,n1:n2,kmax)
      real vx(1:1,n1:n2,kmax),vy(m1:m2,n1:n2,kmax)
      real vz(1:1,n1:n2,kmax)
      real wy(m1:m2,n1:n2,kmax),wz(m1:m2,n1:n2,kmax)
      real txy(1:1,n1:n2,kmax),tzy(1:1,n1:n2,kmax)
      real dxtxy(1:1,n1:n2,kmax),dztzy(1:1,n1:n2,kmax)
      real tyy(m1:m2,n1:n2,kmax)


      a1=4./3.
      a2=2./3.
      d60y137=1./del60y/137.
      mmx=m2-m1+1
      nmx=n2-n1+1

      do k=1,kmax
       do n=n1,n2
        do m=m1,m2
         u(m,n,k)=ru(m,n,k)/rho(m,n,k)
         v(m,n,k)=rv(m,n,k)/rho(m,n,k)
         w(m,n,k)=rw(m,n,k)/rho(m,n,k)
         rvv(m,n,k)=rv(m,n,k)*v(m,n,k)
        enddo
         v2(1,n,k)=v(1,n,k)
       enddo
      enddo

      do k=1,kmax
c      call ddxc6_2(ux(m1,n1,k),u(m1,n1,k),mmx,nmx)
       call ddxc6_2_fb(ux(m1,n1,k),u(m1,n1,k),mmx,nmx,irk,iq,-1)
       call ddxc6_2_fb(vx( 1,n1,k),v2( 1,n1,k), 1,nmx,irk,iq,-1)
c      call ddyc6_2(uy(m1,n1,k),u(m1,n1,k),mmx,nmx)
c      call ddyc6_2(vy(m1,n1,k),v(m1,n1,k),mmx,nmx)
c      call ddyc6_2(wy(m1,n1,k),w(m1,n1,k),mmx,nmx)
       call ddyc6_2_fb(uy(m1,n1,k),u(m1,n1,k),mmx,nmx,irk,iq,-1)
       call ddyc6_2_fb(vy(m1,n1,k),v(m1,n1,k),mmx,nmx,irk,iq,-1)
       call ddyc6_2_fb(wy(m1,n1,k),w(m1,n1,k),mmx,nmx,irk,iq,-1)
      enddo
      call ddzc6_cp_2(vz( 1,n1,1),v2( 1,n1,1),  1,nmx)
      call ddzc6_cp_2(wz(m1,n1,1), w(m1,n1,1),mmx,nmx)

      do k=1,kmax
       do n=n1,n2
         txy(1,n,k)=mu(1,n,k)*(uy(1,n,k)+vx(1,n,k))
         tzy(1,n,k)=mu(1,n,k)*(vz(1,n,k)+wy(1,n,k))
         tyy(1,n,k)=rvv(1,n,k)-mu(1,n,k)*(a1*vy(1,n,k)-
     &                         a2*(ux(1,n,k)+wz(1,n,k)))
        do m=m1+1,m2
         tyy(m,n,k)=p(m,n,k)+rvv(m,n,k)-mu(m,n,k)*(a1*vy(m,n,k)-
     &                         a2*(ux(m,n,k)+wz(m,n,k)))
        enddo
       enddo
      enddo

      do k=1,kmax
        call ddxc6_2_fb(dxtxy(1,n1,k),txy(1,n1,k),1,nmx,irk,iq,-1)
      enddo
      call ddzc6_cp_2(dztzy(1,n1,1),tzy(1,n1,1),1,nmx)

      do k=1,kmax
       do n=n1,n2
         p(1,n,k)=-tyy(1,n,k)  +   !- drvdt(1,n,k)*d60y137 +
     & (300.*tyy(2,n,k)-300.*tyy(3,n,k)+200.*tyy(4,n,k)
     &              -75.*tyy(5,n,k)+ 12.*tyy(6,n,k))/137.
     &             +d60y137*dxtxy(1,n,k)+d60y137*dztzy(1,n,k)
       enddo
      enddo

c     print *,'pwand : ',d60y137
c     print "(8es15.6)",(p(1,n,1),tyy(1,n,1),vy(1,n,1),rvv(1,n,1),
c    &        dxtxy(1,n,1),dztzy(1,n,1),drvdt(1,n,1),
c    &        (300.*tyy(2,n,1)-300.*tyy(3,n,1)+200.*tyy(4,n,1)
c    &         -75.*tyy(5,n,1)+ 12.*tyy(6,n,1))/137.,n=74,94)

      return
      end


