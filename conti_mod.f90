module conti_mod
! Konti-Gleichung

USE Variablen
USE derivatives_x_mod
USE derivatives_y_mod
USE derivatives_z_mod
USE neumann_mod
contains

subroutine conti

IMPLICIT NONE
integer::i,j,k
real::rho_0
	!Ableitung rho_u nach x
rho_0=1.293

	CALL Dphi_Dx_p(rho_u,delta_u,qxi_i,qxi_nr,qxi_r)! !Dphi_Dx_p(phi,sphi,qi_i,qi_nr,qi_r,rel)
!test correction
!delta_u(2,1:m_local,1)=delta_u(2,1:m_local,1)*0.8
!delta_u(3,1:m_local,1)=delta_u(3,1:m_local,1)*1.06
sync all 

!print *, "rho_u entlang X:",rho_u(1,30,1),rho_u(2,30,1),rho_u(3,30,1),rho_u(4,30,1),rho_u(5,30,1)
!print *, "ABLIETUNG ENTLANG X:",delta_u(1,30,1),delta_u(2,30,1),delta_u(3,30,1),delta_u(4,30,1),delta_u(5,30,1)
!	dphi(1:m_local,1:n_local)=solve(1:m_local,1:n_local)

	! Ableitung rho_v nach y
	CALL Dphi_Dy_p(rho_v,delta_v,qyi_i,qyi_nr,qyi_r)
sync all 
!print *, "rho_v entlang X:",rho_v(1,30,1),rho_v(2,30,1),rho_v(3,30,1),rho_v(4,30,1),rho_v(5,30,1)
!print *, "ABLIETUNG ENTLANG X:",delta_v(1,30,1),delta_v(2,30,1),delta_v(3,30,1),delta_v(4,30,1),delta_v(5,30,1)
	! Ableitung rho_w nach z
	CALL Dphi_Dz_p(rho_w,delta_w,qzi_i,qzi_nr,qzi_r)
	! neues rho Berechnen
sync all 

DF_conti(1:m_local,1:n_local,1:o_local)=delta_u(1:m_local,1:n_local,1:o_local)+delta_v(1:m_local,1:n_local,1:o_local)&
+delta_w(1:m_local,1:n_local,1:o_local)

	rho_neu(1:m_local,1:n_local,1:o_local)=rho(1:m_local,1:n_local,1:o_local)&
-dt*0.5*relax*(3.0*DF_conti(1:m_local,1:n_local,1:o_local)-DF_conti_alt(1:m_local,1:n_local,1:o_local))

DF_conti_alt(1:m_local,1:n_local,1:o_local)=DF_conti(1:m_local,1:n_local,1:o_local)
!inkompressibeler Fall
!rho_neu(1:m_local,1:n_local,1:o_local)=rho_0

	CALL SYNC_NEIGHBOURS_P	! rho aktuell

	RETURN

end subroutine


end module

