program main

!**********************************
!* Navier-Stokes and Euler 3D 
!* Co-Array Fortran Solver
!* Friedrich Ulrich 2016
!* aufbauend auf
!* inin
!**********************************

! load the required modules
USE inputfile
USE variablen
USE INI_MOD
USE energy_tot_mod
USE Solver_mod
USE vel_feld_mod
USE druck_feld_mod
USE temp_feld_mod
USE PRINT_ALL_MOD
USE bc_mod
USE en_feld_mod
use TEST_MOD

implicit none
! variables

	! Variable zur Modus-Selektion (Kanalströmung, Taylor-Green etc...)
	INTEGER :: Mode
!	REAL::length_x=  0.001 ,length_y= 1.110446458202763e-04 ,length_z=  0.00000001
	REAL*8::length_x=0.00001 ,length_y=8.189277e-6,length_z=1.0e-3
!REAL::length_x=  0.1 ,length_y= 8.72e-5,length_z=  0.1
!REAL*8::length_x=  0.05e-1 ,length_y= 0.6e-4,length_z=  1

	!! Laufvariablen
	INTEGER :: h,i,j,it,itmax,d
	! Abruchkrit
	REAL*8::energy_temp,epsilon_energy,cfl,Re,simtime,olddt,v_0,w_0,u_help,ma
!Relaxationsfaktor
relax=1.0

!program start

!intro
!choose image one to print only one text
IF (this_image()==1) THEN

	print *, "********************************"
	Print *, "*     _                 _      *"
	print *, "*   ___\ A E R 2 0 1 7 /___    *"
	print *, "*  |CAF - NSG - 3D - SOLVER|   *"
	Print *, "*                              *"
	print *, "********************************"

END IF

! read input file
! Die Variable T_p ist die Anzahl der Punkte T in z Richtung
! diese muss aber mit T-p bezeichnet werden, da T 
! für die Temperatur vorbehalten ist


call inputfileread(M,N,O,R,S,T_p,i,itmax,cfl,Re,Ma,mode)

!Parallel-System erstellen
	myrank=this_image()
!print *, "my-rank",myrank
	numprocs=num_images()
	m_local=M/R
	n_local=N/S
	o_local=O/T_p
	m_kop=m_local/S
	n_kop=n_local/R
	o_kop=o_local/T_p

!print *, "m_local",m_local
!	nlocal=200

	! data 1:m_local, 2 ghostcells
	ALLOCATE(rho(-1:m_local+2,-1:n_local+2,-1:m_local+2)[1:R,1:S,1:*]) 
	ALLOCATE(rho_bc(-1:m_local+2,-1:n_local+2,-1:m_local+2)[1:R,1:S,1:*])
	ALLOCATE(rho_u(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(rho_v(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(rho_w(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
        ALLOCATE(rho_E(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(rho_neu(-1:m_local+2,-1:n_local+2,-1:m_local+2)[1:R,1:S,1:*]) 
	ALLOCATE(rho_u_neu(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(rho_v_neu(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(rho_w_neu(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
        ALLOCATE(rho_E_neu(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(rho_alt(-1:m_local+2,-1:n_local+2,-1:m_local+2)[1:R,1:S,1:*]) 
	ALLOCATE(rho_u_alt(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(rho_v_alt(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(rho_w_alt(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
        ALLOCATE(rho_E_alt(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
        ALLOCATE(E_plus_p(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])


	ALLOCATE(E_save(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(T_save(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(rho_save(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(u_save(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(v_save(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(w_save(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(p(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(E(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(Epu(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
        ALLOCATE(Epv(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
        ALLOCATE(Epw(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])

	ALLOCATE(u(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(u_bc(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(v_bc(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])

	ALLOCATE(v(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(w(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(T(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(T_bc(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(vis(1:m_local,1:n_local,1:o_local)[1:R,1:S,1:*])
	ALLOCATE(mu(1:m_local,1:n_local,1:o_local)[1:R,1:S,1:*])
	ALLOCATE(k(1:m_local,1:n_local,1:o_local)[1:R,1:S,1:*])
	ALLOCATE(dphi(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(dphi1(1:m_local,1:n_local,1:o_local)[1:R,1:S,1:*])
	ALLOCATE(phi(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(sphi(1:m_local,1:n_local,1:o_local)[1:R,1:S,1:*])
	ALLOCATE(dphi4(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(dphi2(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(dphi3(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(DF_energy_alt(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(DF_conti_alt(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(DF_zmom_alt(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(DF_ymom_alt(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(DF_xmom_alt(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(DF_energy(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(DF_conti(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(DF_zmom(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(DF_ymom(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(DF_xmom(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])



	ALLOCATE(solve(1:m_local,1:n_local,1:o_local)[1:R,1:S,1:*])
   	ALLOCATE(E_k(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
   	ALLOCATE(a(1:m_local,1:n_local,1:o_local)[1:R,1:S,1:*])
	ALLOCATE(uinit(1:nlocal,1:nlocal,1:o_local)[1:R,1:S,1:*])
	ALLOCATE(rinit(1:nlocal,1:nlocal,1:o_local)[1:R,1:S,1:*])
	ALLOCATE(vinit(1:nlocal,1:nlocal,1:o_local)[1:R,1:S,1:*])
	ALLOCATE(tinit(1:nlocal,1:nlocal,1:o_local)[1:R,1:S,1:*])
	!!
	ALLOCATE(du(1:m_local,1:n_local,1:o_local)[1:R,1:S,1:*])
	ALLOCATE(dv(1:m_local,1:n_local,1:o_local)[1:R,1:S,1:*])
	ALLOCATE(drudx(1:m_local,1:n_local,1:o_local)[1:R,1:S,1:*])
	ALLOCATE(drvdy(1:m_local,1:n_local,1:o_local)[1:R,1:S,1:*])
	ALLOCATE(dudx(1:m_local,1:n_local,1:o_local)[1:R,1:S,1:*])
	ALLOCATE(dvdx(1:m_local,1:n_local,1:o_local)[1:R,1:S,1:*])
	ALLOCATE(dvdy(1:m_local,1:n_local,1:o_local)[1:R,1:S,1:*])
	ALLOCATE(dudy(1:m_local,1:n_local,1:o_local)[1:R,1:S,1:*])
	ALLOCATE(poisson(1:m_local,1:n_local,1:o_local)[1:R,1:S,1:*])
	ALLOCATE(DDX_DDY(1:m_local,1:n_local,1:o_local)[1:R,1:S,1:*])

	!Gleichungslöser in x

	ALLOCATE(b_x(1:m_local),c_x(1:m_local))
	ALLOCATE(a_x(1:m_local+1)[1:R,1:S,1:*],f_x(1:m_local+1)[1:R,1:S,1:*],g_x(1:m_local+1)[1:R,1:S,1:*])
	ALLOCATE(y_x(1:m_local+1,1:n_local,1:2)[1:R,1:S,1:*],x_x(0:m_local,1:n_local,1:2)[1:R,1:S,1:*])!????????????????????????
	ALLOCATE(a_x_s(1:R)[1:R,1:S,1:*],f_x_s(1:R)[1:R,1:S,1:*],g_x_s(1:R)[1:R,1:S,1:*])
	ALLOCATE(y_x_s(1:R,1:n_kop,1:2)[1:R,1:S,1:*],x_x_s(1:R,1:n_kop,1:2)[1:R,1:S,1:*])
	ALLOCATE(alpha_x(1:n_local,1:2)[1:R,1:S,1:*],beta_x(1:n_local)[1:R,1:S,1:*],z_x(1:m_local))	

	!Gleichungslöser in y
	ALLOCATE(b_y(1:n_local),c_y(1:n_local))
	ALLOCATE(a_y(1:n_local+1)[1:R,1:S,1:*],f_y(1:n_local+1)[1:R,1:S,1:*],g_y(1:n_local+1)[1:R,1:S,1:*])
	ALLOCATE(y_y(1:m_local,1:n_local+1,1:2)[1:R,1:S,1:*],x_y(1:m_local,0:n_local,1:2)[1:R,1:S,1:*])!????????????????????????
	ALLOCATE(a_y_s(1:S)[1:R,1:S,1:*],f_y_s(1:S)[1:R,1:S,1:*],g_y_s(1:S)[1:R,1:S,1:*])
	ALLOCATE(y_y_s(1:m_kop,1:S,1:2)[1:R,1:S,1:*],x_y_s(1:m_kop,1:S,1:2)[1:R,1:S,1:*])
	ALLOCATE(alpha_y(1:m_local,1:2)[1:R,1:S,1:*],beta_y(1:m_local)[1:R,1:S,1:*],z_y(1:n_local))

	!Gleichungslöser i z
	ALLOCATE(b_z(1:o_local),c_z(1:o_local))
	ALLOCATE(a_z(1:o_local+1)[1:R,1:S,1:*],f_z(1:o_local+1)[1:R,1:S,1:*],g_z(1:o_local+1)[1:R,1:S,1:*])
	ALLOCATE(y_z(1:m_local,1:n_local+1,1:2)[1:R,1:S,1:*],x_z(1:m_local,0:n_local,1:2)[1:R,1:S,1:*])!????????????????????????
	ALLOCATE(a_z_s(1:S)[1:R,1:S,1:*],f_z_s(1:S)[1:R,1:S,1:*],g_z_s(1:S)[1:R,1:S,1:*])
	ALLOCATE(y_z_s(1:m_kop,1:S,1:2)[1:R,1:S,1:*],x_z_s(1:m_kop,1:S,1:2)[1:R,1:S,1:*])
	ALLOCATE(alpha_z(1:m_local,1:2)[1:R,1:S,1:*],beta_z(1:m_local)[1:R,1:S,1:*],z_z(1:o_local))
	
	!! Spannungstensor
	ALLOCATE(tau_xx(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(tau_yy(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(tau_zz(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(tau_xy(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(tau_xz(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(tau_zy(-1:m_local+2,-1:n_local+2,-1:o_local+2)[1:R,1:S,1:*])
	ALLOCATE(delta_u(1:m_local,1:n_local,1:o_local)[1:R,1:S,1:*])
	ALLOCATE(delta_v(1:m_local,1:n_local,1:o_local)[1:R,1:S,1:*])
	ALLOCATE(delta_w(1:m_local,1:n_local,1:o_local)[1:R,1:S,1:*])

    !! Koordinaten des ausführenden Image bestimmen

	DO h=1,R
		DO i=1,S
			DO j=1,T_p
				IF(image_index(rho,[h,i,j])==myrank)THEN
					m_pos=h
					n_pos=i
					o_pos=j
					EXIT
				ENDIF
			ENDDO
		ENDDO
	ENDDO
    print *, 'myrank: ',myrank, 'm_pos:',m_pos,'n_pos: ',n_pos,'o_pos:'&
 ,o_pos,'procCount:',numprocs


	!! Synchronisierungs-Variablen (sync_n_up,sync_m_up) belegen
	IF(R==1)THEN
		sync_m_up=myrank
		sync_m_down=myrank
	ELSE
		IF(m_pos<R)sync_m_up=image_index(rho,[m_pos+1,n_pos,o_pos])
		IF(m_pos>1)sync_m_down=image_index(rho,[m_pos-1,n_pos,o_pos])
	ENDIF
	IF(S==1)THEN
		sync_n_up=myrank
		sync_n_down=myrank
	ELSE
		IF(n_pos<S)sync_n_up=image_index(rho,[m_pos,n_pos+1,o_pos])
		IF(n_pos>1)sync_n_down=image_index(rho,[m_pos,n_pos-1,o_pos])
	ENDIF
	IF(T_p==1)THEN
		sync_o_up=myrank
		sync_o_down=myrank
	ELSE
		IF(o_pos<T_p)sync_o_up=image_index(rho,[m_pos,n_pos,o_pos+1])
		IF(o_pos>1)sync_o_down=image_index(rho,[m_pos,n_pos,o_pos-1])
	ENDIF

	ALLOCATE(sync_m_array(1:R))
	DO i=1,R
		sync_m_array(i)=image_index(rho,[i,n_pos,o_pos])
	ENDDO
	ALLOCATE(sync_n_array(1:S))
	DO i=1,S
		sync_n_array(i)=image_index(rho,[m_pos,i,o_pos])
	ENDDO	
	ALLOCATE(sync_o_array(1:T_p))
	DO i=1,T_p
		sync_o_array(i)=image_index(rho,[m_pos,n_pos,i])
	ENDDO
	image_m_1=image_index(rho,[1,n_pos,o_pos])
	image_n_1=image_index(rho,[m_pos,1,o_pos])
	image_o_1=image_index(rho,[m_pos,n_pos,1])
	image_m_max=image_index(rho,[R,n_pos,o_pos])
	image_n_max=image_index(rho,[m_pos,S,o_pos])
	image_o_max=image_index(rho,[m_pos,n_pos,T_p])

	!Fall erstellen	
  	dx = length_x / (M-1)
   	dy = length_y / (N-1)
	dz = length_z / (O-1) 
!

! u_0=Ma*sqrt(gamma*R*T_0)
!	u_0=Ma*sqrt(1.4*287*273)
!u_0=0.11495E+03	
!v_0=0.1
!w_0=0.1
!u_0=231.8
u_0=1361.12
v_0=0.38
w_0=0.1
mu0=1.458d-6
print *, " dx,dy,dz:",dx,dy,dz
!old version
  	dt = cfl*min(dx/abs(u_0),dy/abs(u_0),dz/abs(u_0)) !v_0=u_0
!dt =cfl*dx*dx/mu0
!dt=0.5e-10
print *, "delta t ", dt
olddt=dt
simtime=0.0
!mu0=1.458d-6
!	Re=u_0*length_x*mu0
!print *,"dt,Re, u0:", dt,Re, u_0
!	print *,'dt::::::::::::::',dt,'dx/abs(u_0):',dx/abs(u_0),'dy/abs(u_0):',dy/abs(u_0),&
!'dz/abs(u_0):',dz/abs(u_0)
	epsilon_energy=1e-06

!	IF(m_pos==1.AND.n_pos==1.AND.o_pos==1)PRINT *,'dx: ',dx,' dy: ',dy,' dz: ',dz,' dt: ',dt,'Re: ',Re
	!kompakte Differenzen
	CALL INITIAL_COEFF
!	!Anfangswerte	
print *, "reading initial condition"
	CALL INITIAL_CONDITION(mode,dx,dy)
	SYNC ALL


! starting the calculation
	IF(m_pos==1.AND.n_pos==1.AND.o_pos==1)PRINT *,'Berechnung startet!'

it=0

rho_save=rho
T_save=T
E_save=E
u_save=u
v_save=v
w_save=w
!CALL BC(dx,dy)
simtime=0

DF_energy_alt(-1:m_local+2,-1:n_local+2,-1:o_local+2)=0.0
DF_conti_alt(-1:m_local+2,-1:n_local+2,-1:o_local+2)=0.0
DF_zmom_alt(-1:m_local+2,-1:n_local+2,-1:o_local+2)=0.0
DF_ymom_alt(-1:m_local+2,-1:n_local+2,-1:o_local+2)=0.0
DF_xmom_alt(-1:m_local+2,-1:n_local+2,-1:o_local+2)=0.0

poisson(1:m_local,1:n_local,1:o_local)=rho(1:m_local,1:n_local,1:o_local)
DO WHILE(it<itmax)

	it=it+1
!print *, "####STARTVALUES####"
!print *, "Energy1", rho_E(m_local-2,1,1), rho_E(m_local-1,1,1), rho_E(m_local,1,1)
!print *, "Pressu1",   (mu(1,100,1)*1e5)/(u(1,100,1)*rho(1,100,1))
!print *, "temp--1", T(m_local-2,1,1), T(m_local-1,1,1), T(m_local,1,1)
!print *, "rho_u-1", rho_u(m_local-2,1,1), rho_u(m_local-1,1,1), rho_u(m_local,1,1)
!print *, "rho_v-1", rho_v(m_local-2,77,1), rho_v(m_local-1,77,1), rho_v(m_local,77,1)
!print *, "rho---1", rho(m_local-2,1,1), rho(m_local-1,1,1), rho(m_local,1,1)

!print *, "p:bsolver",rho_E(2,1,1),rho_E(2,2,1)

sync all

	! Energie, Implsgleichungen etc...
sync all	
	CALL Solver(dx,dy)

sync all

	! Geschwindigkeitsfelder werden aktualisiert
	CALL VEL_FELD

SYNC ALL
!CALL BC(dx,dy)
	!Energiefelder werden aktualisiert
CALL EN_FELD
SYNC ALL


	!Temperatur aus Energie- und GGeschwindigkeitsfeld 	
CALL TEMP_FELD

SYNC ALL


! Druck aus dem idealen Gas-Gesetz
CALL DRUCK_FELD


sync all

CALL BC(dx,dy)




		! Abbruchkrit
		! energy_temp=total_energy
	!	CALL CALC_ENERGY_KIN
sync all			
		CALL CALC_ENERGY
		IF(m_pos==1.AND.n_pos==1.AND.o_pos==1)THEN
	
			OPEN(unit=34,file='convergence.dat')
			if(it==1)then

			! write convernge file header
!			WRITE(34,FMT='(A25)') 'TITLE = "convergence.dat"'
!			WRITE(34,FMT='(A11,A3,A1,A3,A1,A3,A1,A12)')'VARIABLES =','"x"',',','"y"',',',&
!			&'"z"',',','"Energynorm"'
!			WRITE(34,FMT='(A8,I4,A5,I4,A4,I4,A4,I4)') 'ZONE T="',M,'", I=',itmax,', J=',1,', K=',1
			end if

			PRINT *,'iterations: ',it,'total_energy: ',total_energy

v_0=0.0
do i=1,m_local
do j=1,n_local
do h=1,o_local
v_0=max(v_0,abs(poisson(i,j,h)-rho(i,j,h)))
enddo
enddo
enddo

print *, "Max. Fehler:",v_0

!if(it.NE.1.AND.v_0<1e-8)then
!it=itmax+1
!endif

poisson(1:m_local,1:n_local,1:o_local)=rho(1:m_local,1:n_local,1:o_local)
			WRITE(34,FMT='(F22.10,A1,F22.10,A1,F22.10,A1,F22.10)')it*1.0,' ',it*1.0,' ',1.0,' ',v_0

ENDIF
sync all
!print *, "p:afterEn",rho_E(2,1,1),rho(2,2,1)
if(mod(it,5000)==0)then
print *, "new cfl-time"

do i=1,m_local
do j=1,n_local
do h=1,o_local
u_0=max(u_0,u(i,j,h))
enddo
enddo
enddo
 print *, "max u:",u_0
do i=1,m_local
do j=1,n_local
do h=1,o_local
v_0=max(v_0,v(i,j,h))
enddo
enddo
enddo
 print *, "max v:",v_0
do i=1,m_local
do j=1,n_local
do h=1,o_local
w_0=max(w_0,w(i,j,h))
enddo
enddo
enddo
  print *, "max w:",w_0
u_help=max(u_0,v_0,w_0)
cfl=0.03
 dt = cfl*min(dx/abs(u_help),dy/abs(u_help),dz/abs(u_help))
print *, "new dt", dt
print *, "Abweichung dt:",1.0-(olddt-dt)/olddt
print *, "**********************************"
end if



simtime=simtime+dt
sync all
ENDDO


IF(m_pos==1.AND.n_pos==1.AND.o_pos==1)THEN
			PRINT *,"writing result.dat file..."
		ENDIF

 CALL PRINT_ALL

IF(m_pos==1.AND.n_pos==1.AND.o_pos==1)THEN
			PRINT *,"result.dat file complete"
		ENDIF

!Testroutine für ableitungen
! call test(dx,dy,dz)

! delocalisieren

	DEALLOCATE(rho,rho_u,rho_v,rho_w,rho_E,E,p,Epu,Epv,u,v,T,E_k,vis,mu,a)

	DEALLOCATE(dphi,dphi1,dphi2,dphi3,dphi4,solve,sync_m_array,sync_n_array,du,dv,uinit,rinit,vinit,tinit)!,arr1,arr2,arr3,arr4,arr5,arr6
	DEALLOCATE(a_x,b_x,c_x,y_x,f_x,g_x,x_x,a_x_s,f_x_s,g_x_s,y_x_s,x_x_s,z_x,alpha_x,beta_x)
	DEALLOCATE(a_y,b_y,c_y,y_y,f_y,g_y,x_y,a_y_s,f_y_s,g_y_s,y_y_s,x_y_s,z_y,alpha_y,beta_y)

IF(m_pos==1.AND.n_pos==1.AND.o_pos==1)THEN
			print *, "end of program"
print *,"Simulationtime:",simtime
!print *, "dyn.Viskosität:", mu(1,n_local,-1)
!print *,"CFL-Zahl:",cfl

ENDIF

end program
