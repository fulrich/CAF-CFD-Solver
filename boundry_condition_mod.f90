module bc_mod
USE Variablen
USE sync_mod
USE P_wall_mod
CONTAINS

subroutine bc(dx,dy)
integer ::i,j
real*8::p_0, gamma,rho_0,dummy,dx,dy
p_0=101325.0
rho_0=1.293
gamma=1.4

sync all 

!################################
IF(m_pos==1)THEN
! Einströmrand x=1

u(1,1:n_local,-1:o_local+2)=4.0*340.28
!u_bc(1,1:n_local,-1:o_local+2)
!print *, "Eintrittsgeschwindigkeit", u_bc(1,2,1),m_pos,n_pos
v(1,1:n_local,-1:o_local+2)=0.0
!v_bc(1,1:n_local,-1:o_local+2)
w(1,1:n_local,-1:o_local+2)=0.0

T(1,1:n_local,1:o_local)=288.16

!T_bc(1,1:n_local,1:o_local)

rho(1,1:n_local,1:o_local)=101325.0/(287.0*288.16)
!rho_bc(1,1:n_local,1:o_local)

rho_w(1,1:n_local,1:o_local)=0.0


p(1,1:n_local,-1:o_local+2)=101325.0
!rho(1,1:n_local,-1:o_local+2)&
!*287.0*T(1,1:n_local,-1:o_local+2)


rho_u(1,1:n_local,-1:o_local+2)=u(1,1:n_local,-1:o_local+2)&
*rho(1,1:n_local,-1:o_local+2)

rho_v(1,1:n_local,-1:o_local+2)=v(1,1:n_local,-1:o_local+2)&
*rho(1,1:n_local,-1:o_local+2)

rho_E(1,-1:n_local+2,-1:o_local+2)=(&
0.5*(rho_u(1,-1:n_local+2,-1:o_local+2)&
**2+rho_v(1,-1:n_local+2,-1:o_local+2)**2&
+rho_w(1,-1:n_local+2,-1:o_local+2)**2)&
+rho(1,-1:n_local+2,-1:o_local+2)**2*&
T(1,-1:n_local+2,-1:o_local+2)*717.5)/rho(1,-1:n_local+2,-1:o_local+2)

E(1,-1:n_local+2,-1:o_local+2)=&
rho_E(1,-1:n_local+2,-1:o_local+2)/&
rho(1,-1:n_local+2,-1:o_local+2)

END IF
sync all
!##################################
! freestream
IF(n_pos==S)THEN
u(1:m_local,n_local,-1:o_local+2)=4.0*340.28
!u_bc(1:m_local,n_local,-1:o_local+2)
v(1:m_local,n_local,-1:o_local+2)=0.0
!v_bc(1:m_local,n_local,-1:o_local+2)
w(1:m_local,n_local,-1:o_local+2)=0.0

rho(1:m_local,n_local,-1:o_local+2)=101325.0/(287.0*288.16)

rho_u(1:m_local,n_local,-1:o_local+2)=u(1:m_local,n_local,-1:o_local+2)&
*rho(1:m_local,n_local,-1:o_local+2)

rho_v(1:m_local,n_local,-1:o_local+2)=v(1:m_local,n_local,-1:o_local+2)&
*rho(1:m_local,n_local,-1:o_local+2)

rho_w(1:m_local,n_local,-1:o_local+2)=0.0

p(1:m_local,n_local,-1:o_local+2)=&
101325.0
!rho(1,n_local,1)*287.0*T(1,n_local,1)


T(1:m_local,n_local,-1:o_local+2)=288.16
!T_bc(1:m_local,n_local,-1:o_local+2)



!p(1:m_local,n_local,-1:o_local+2)&
!/287.0/T(1:m_local,n_local,-1:o_local+2)


rho_E(1:m_local,n_local,-1:o_local+2)=(&
0.5*(rho_u(1:m_local,n_local,-1:o_local+2)&
**2+rho_v(1:m_local,n_local,-1:o_local+2)**2&
+rho_w(1:m_local,n_local,-1:o_local+2)**2)&
+rho(1:m_local,n_local,-1:o_local+2)**2*&
T(1:m_local,n_local,-1:o_local+2)*717.5)/rho(1:m_local,n_local,-1:o_local+2)

E(1:m_local,n_local,-1:o_local+2)=&
rho_E(1:m_local,n_local,-1:o_local+2)/&
rho(1:m_local,n_local,-1:o_local+2)

END IF
sync all
!##################################
! Platte y=1
IF(n_pos==1)THEN

rho_u(1:m_local,1,-1:o_local+2)=1.293*0.0
rho_v(1:m_local,1,-1:o_local+2)=1.293*0.0
rho_w(1:m_local,1,-1:o_local+2)=0.0
u(1:m_local,1,-1:o_local+2)=0.0
v(1:m_local,1,-1:o_local+2)=0.0
w(1:m_local,1,-1:o_local+2)=0.0
T(1:m_local,1,-1:o_local+2)=288.16
!T_bc(1:m_local,1,-1:o_local+2)


p(1:m_local,1,-1:o_local+2)=(2.0*p(1:m_local,2,-1:o_local+2)&
-p(1:m_local,3,-1:o_local+2))

rho(1:m_local,1,-1:o_local+2)=p(1:m_local,1,-1:o_local+2)&
/287.0/T(1:m_local,1,-1:o_local+2)



rho_E(1:m_local,1,-1:o_local+2)=(&
0.5*(rho_u(1:m_local,1,-1:o_local+2)&
**2+rho_v(1:m_local,1,-1:o_local+2)**2&
+rho_w(1:m_local,1,-1:o_local+2)**2)&
+rho(1:m_local,1,-1:o_local+2)**2*&
T(1:m_local,1,-1:o_local+2)*717.5)/rho(1:m_local-1,1,-1:o_local+2)

E(1:m_local,1,-1:o_local+2)=&
rho_E(1:m_local,1,-1:o_local+2)/&
rho(1:m_local,1,-1:o_local+2)

END IF

sync all
!################################
! Plattenkante x=1 y=1
IF(n_pos==1.AND.m_pos==1)THEN

u(1,1,-1:o_local+2)=4.0*340.28
!u_bc(1,1,-1:o_local+2)
v(1,1,-1:o_local+2)=0.0
!v_bc(1,1,-1:o_local+2)
w(1,1,-1:o_local+2)=0.0

T(1,1,-1:o_local+2)=288.16
!T_bc(1,1,-1:o_local+2)

rho(1,1,-1:o_local+2)=rho_bc(1,1,-1:o_local+2)


rho_w(1,1,-1:o_local+2)=0.0


p(1,1,-1:o_local+2)=&
rho(1,1,-1:o_local+2)&
*287.0*T(1,1,-1:o_local+2)


rho_u(1,1,-1:o_local+2)=u(1,1,-1:o_local+2)&
*rho(1,1,-1:o_local+2)

rho_v(1,1:n_local,-1:o_local+2)=v(1,1:n_local,-1:o_local+2)&
*rho(1,1:n_local,-1:o_local+2)

rho_E(1,-1:n_local+2,-1:o_local+2)=(&
0.5*(rho_u(1,-1:n_local+2,-1:o_local+2)&
**2+rho_v(1,-1:n_local+2,-1:o_local+2)**2&
+rho_w(1,-1:n_local+2,-1:o_local+2)**2)&
+rho(1,-1:n_local+2,-1:o_local+2)**2*&
T(1,-1:n_local+2,-1:o_local+2)*717.5)/rho(1,-1:n_local+2,-1:o_local+2)

E(1,-1:n_local+2,-1:o_local+2)=&
rho_E(1,-1:n_local+2,-1:o_local+2)/&
rho(1,-1:n_local+2,-1:o_local+2)

END IF
sync all
!################################
! Austritt x=M

IF(m_pos==R)THEN
p(m_local,1:n_local,1:o_local)=(2.0*p(m_local-1,1:n_local,1:o_local)&
-p(m_local-2,1:n_local,1:o_local))


T(m_local,1:n_local,1:o_local)=2.0*T(m_local-1,1:n_local,1:o_local)&
-T(m_local-2,1:n_local,1:o_local)

rho_u(m_local,1:n_local,1:o_local)=2.0*rho_u(m_local-1,1:n_local,1:o_local)&
-rho_u(m_local-2,1:n_local,1:o_local)

rho_v(m_local,1:n_local,1:o_local)=2.0*rho_v(m_local-1,1:n_local,1:o_local)&
-rho_v(m_local-2,1:n_local,1:o_local)


rho(m_local,1:n_local,-1:o_local+2)=p(m_local,1:n_local,-1:o_local+2)/(287.0*&
T(m_local,1:n_local,-1:o_local+2))


rho_E(m_local,1:n_local,-1:o_local+2)=(&
0.5*(rho_u(m_local,1:n_local,-1:o_local+2)&
**2+rho_v(m_local,1:n_local,-1:o_local+2)**2&
+rho_w(m_local,1:n_local,-1:o_local+2)**2)&
+(rho(m_local,1:n_local,-1:o_local+2)**2)*&
T(m_local,1:n_local,-1:o_local+2)*717.5)/rho(m_local,1:n_local,-1:o_local+2)


E(m_local,1:n_local,-1:o_local+2)=&
rho_E(m_local,1:n_local,-1:o_local+2)/&
rho(m_local,1:n_local,-1:o_local+2)

END IF
sync all
!#####################
! platten Hinterkante

IF(m_pos==R.AND.n_pos==1)THEN

rho_u(m_local,1,-1:o_local+2)=1.293*0.0
rho_v(m_local,1,-1:o_local+2)=1.293*0.0
rho_w(m_local,1,-1:o_local+2)=0.0
u(m_local,1,-1:o_local+2)=0.0
v(m_local,1,-1:o_local+2)=0.0
w(m_local,1,-1:o_local+2)=0.0
T(m_local,1,-1:o_local+2)=288.16
!T_bc(m_local,1,-1:o_local+2)


p(m_local,1,1:o_local)=(2.0*p(m_local,2,1:o_local)&
-p(m_local,3,1:o_local))


rho(m_local,-1:1,-1:o_local+2)=p(m_local,-1:1,-1:o_local+2)&
/287.0/T(m_local,-1:1,-1:o_local+2)



rho_E(m_local,1,-1:o_local+2)=(&
0.5*(rho_u(m_local,1,-1:o_local+2)&
**2+rho_v(m_local,1,-1:o_local+2)**2&
+rho_w(m_local,1,-1:o_local+2)**2)&
+rho(m_local,1,-1:o_local+2)**2*&
T(m_local,1,-1:o_local+2)*717.5)/rho(m_local-1,1,-1:o_local+2)

E(m_local,1,-1:o_local+2)=&
rho_E(m_local,1,-1:o_local+2)/&
rho(m_local,1,-1:o_local+2)

END IF
sync all
!############################
! Freistrom Hinterkante
IF(n_pos==S.AND.m_pos==R)THEN
u(m_local,n_local,-1:o_local+2)=4.0*340.28
!u_bc(m_local,n_local,-1:o_local+2)
v(m_local,n_local,-1:o_local+2)=0.0
!v_bc(m_local,n_local,-1:o_local+2)
w(m_local,n_local,-1:o_local+2)=0.0

rho(m_local,n_local,-1:o_local+2)=p(m_local,n_local,-1:o_local+2)&
/287.0/T(m_local,n_local,-1:o_local+2)

rho_u(m_local,n_local,-1:o_local+2)=u(m_local,n_local,-1:o_local+2)&
*rho(m_local,n_local,-1:o_local+2)

rho_v(m_local,n_local,-1:o_local+2)=v(m_local,n_local,-1:o_local+2)&
*rho(m_local,n_local,-1:o_local+2)

rho_w(m_local,n_local,-1:o_local+2)=0.0

p(m_local,n_local,-1:o_local+2)=101325.0
!&
!rho(m_local,n_local,1)*287.0*T(1,n_local,1)


T(m_local,n_local,-1:o_local+2)=288.16
!T_bc(m_local,n_local,-1:o_local+2)





rho_E(m_local,n_local,-1:o_local+2)=(&
0.5*(rho_u(m_local,n_local,-1:o_local+2)&
**2+rho_v(m_local,n_local,-1:o_local+2)**2&
+rho_w(m_local,n_local,-1:o_local+2)**2)&
+rho(m_local,n_local,-1:o_local+2)**2*&
T(m_local,n_local,-1:o_local+2)*717.5)/rho(m_local,n_local,-1:o_local+2)

E(m_local,n_local,-1:o_local+2)=&
rho_E(m_local,n_local,-1:o_local+2)/&
rho(m_local,n_local,-1:o_local+2)

END IF



SYNC ALL



end subroutine

end module
